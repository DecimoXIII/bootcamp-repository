'use strict';
module.exports = function(app) {
  var todoList = require('../controllers/bootCampController');

  // todoList Routes
  app.route('/usuarios')///5d6d7a584504a9cb4fd63768
    .get(todoList.list_all_tasks)
    .post(todoList.create_a_task);


  app.route('/movimientos/:taskId')
    .get(todoList.read_a_task)
    .put(todoList.update_a_task)
    .delete(todoList.delete_a_task);
};
