'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var MovimientosSchema = new Schema({
  nombre: String,
  apellidoP: String,
  apellidoM: String,
  cuentas: [{tipoCuenta: String, montoActual: Number, nTarjeta: String}],
  ofertas: [{lda: String, mensaje: String, monto: Number}],
  movimientos: [{fecha:{type: Date, default: Date.now}, monto: Number, comercio: String, ubicacion: { latitud: String, longitud: String}}]
});


// var TaskSchema = new Schema({
//   name: {
//     type: String,
//     required: 'Kindly enter the name of the task'
//   },
//   Created_date: {
//     type: Date,
//     default: Date.now
//   },
//   status: {
//     type: [{
//       type: String,
//       enum: ['pending', 'ongoing', 'completed']
//     }],
//     default: ['pending']
//   }
// });


// {
// "monto": 1000,
// "comercio": "Sears",
// "ubicacion":{"latitud": "-34.397", "longitud": "150.644"}
// }

// {
// "nombre": "Pancho Villa",
// "apellidoP": "Pérez",
// "apellidoM": "Sánchez"
// }

module.exports = mongoose.model('Movimientos', MovimientosSchema);
